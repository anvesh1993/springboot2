package com.example.swagger;


import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;





	public class StudentService {

		@Autowired
		private StudentRepository studentRepository;

		public Student saveStudent(Student student) {
			return studentRepository.save(student);
		}
		public List<Student> getAllStudents() {
			return studentRepository.findAll();
		}

		public List<Student> findStudentByAge(int age) {
			return studentRepository.findByAge(age);
		}

		public List<Student> findStudentById(int id) {
			return studentRepository.findById(id);
		}

		public List<Student> findStudentByIdAndAge(int id, int age) {
			return studentRepository.findByIdAndAge(id, age);
		}

		public List<Student> findStudentByIdOrAge(int id, int age) {
			return studentRepository.findByIdOrAge(id, age);
		}

		public List<Student> findStudentByAgeLessThan(int age) {
			return studentRepository.findByAgeLessThan(age);
		}

		public List<Student> findStudentByAgeLessThanEqual(int age) {
			return studentRepository.findByAgeLessThanEqual(age);
		}

		public List<Student> findStudentByAgeGreaterThan(int age) {
			return studentRepository.findByAgeGreaterThan(age);
		}

		public List<Student> findStudentByAgeGreaterThanEqual(int age) {
			return studentRepository.findByAgeGreaterThanEqual(age);
		}

		public List<Student> findStudentByAgeIsNull() {
			return studentRepository.findByAgeIsNull();
		}

		public List<Student> findStudentByNameIsNull() {
			return studentRepository.findByNameIsNull();
		}

		public Student updateStudent(Integer id, Student student) {
			student.setId(id);
			return studentRepository.save(student);
		}

		public void deleteStudent(Integer id) {
			studentRepository.deleteById(id);
		}

		public Student partialStudentUpdate(Integer id, Student student) {
			student.setId(id);
			Optional<Student> studentFromDb = studentRepository.findById(id);
			if (studentFromDb.isPresent()) {
				Student temp = studentFromDb.get();
				Util.myCopyProperties(student, temp);
				return studentRepository.save(temp);
			} else {
				return null;
			}
		}

		public List<Student> findStudentByAgeBetween(int from, int to) {
			return studentRepository.findByAgeBetween(from, to);
		}

		public List<Student> findStudentByNameIs(String name) {
			return studentRepository.findByNameIs(name);
		}

		public List<Student> findStudentByNameEquals(String name) {
			return studentRepository.findByNameEquals(name);
		}

		public List<Student> findStudentByNameIsNot(String name) {
			return studentRepository.findByNameIsNot(name);
		}

		public List<Student> findStudentByNameIsNotNull() {
			return studentRepository.findByNameIsNotNull();
		}

		public List<Student> findStudentByNameLike(String likePattern) {
			return studentRepository.findByNameLike(likePattern);
		}

		public List<Student> findStudentAllByOrderById() {
			return studentRepository.findAllByOrderById();
		}

		public List<Student> findStudentAllByOrderByIdAsc() {
			return studentRepository.findAllByOrderByIdAsc();
		}

		public List<Student> findStudentAllByOrderByIdDesc() {
			return studentRepository.findAllByOrderByIdDesc();
		}

		
	}


