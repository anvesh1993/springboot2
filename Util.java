package com.example.swagger;


import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

public class Util {
	public static String[] getNullPropertyNames(Object source) {
		final BeanWrapper src = new BeanWrapperImpl(source);
		java.beans.PropertyDescriptor[] pds = src.getPropertyDescriptors();

		Set<String> emptyNames = new HashSet<String>();
		for (java.beans.PropertyDescriptor pd : pds) {
			Object srcValue = src.getPropertyValue(pd.getName());
			if (srcValue == null)
				emptyNames.add(pd.getName());
		}
		String[] result = new String[emptyNames.size()];
		String[] res = emptyNames.toArray(result);
		return res;
	}
	public static void myCopyProperties(Object src, Object target) {
		BeanUtils.copyProperties(src, target, getNullPropertyNames(src));
	}

	public static boolean isPagbleRequest(Integer pageNumber, Integer numberOfPages) {
		return (pageNumber != null && numberOfPages != null) ? true : false;
	}

	public static Pageable pageObject(Integer pageNumber, Integer noOfRecords) {
		return PageRequest.of(pageNumber, noOfRecords);
	}

}



