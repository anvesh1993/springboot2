package com.example.swagger;



import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

public class Controller {
	@Autowired
	StudentService studentService;
	/*
	 * @Autowired private StudentRepository studentRepository;
	 */
	List<Student> list = new ArrayList<>();

	/*
	 * @GetMapping(value = "/students", produces = {
	 * MediaType.APPLICATION_JSON_VALUE, }) public ResponseEntity<List<Student>>
	 * getStudents(@RequestParam(name = "age", required = false) Integer age) {
	 * List<Student> students = studentService.getAllStudents(); return
	 * ResponseEntity.status(200).body(students); }
	 */

	@PostMapping(value = "students", consumes = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<Student> saveStudent(@RequestBody Student student) {
		Student s = studentService.saveStudent(student);
		return ResponseEntity.status(201).body(s);
	}

	/*
	 * @PutMapping("students/{id}") public ResponseEntity<Student>
	 * updateStudent(@PathVariable("id") Integer id, @RequestBody Student student) {
	 * for (Student student1 : listupdateStudent) { if (student1.getId() == id) {
	 * list.remove(student1); list.add(student); break; } } return
	 * ResponseEntity.status(200).body(student); }
	 */

	/*
	 * @DeleteMapping("students/{id}") public ResponseEntity<Void>
	 * deleteStudent(@PathVariable("id") Integer id) { for (Student student1 : list)
	 * { if (student1.getId() == id) { list.remove(student1); break; } } return
	 * ResponseEntity.status(204).build(); }
	 */

	@GetMapping(value = "/students", produces = { MediaType.APPLICATION_JSON_VALUE, })
	public ResponseEntity<List<Student>> getStudentsage(@RequestParam(name = "age", required = false) Integer age) {
		if (age != null) {
			List<Student> students = studentService.findStudentByAge(age);
			return ResponseEntity.status(200).body(students);
		} else {
			List<Student> students = studentService.getAllStudents();
			return ResponseEntity.status(200).body(students);

		}
	}

	@GetMapping(value = "/students1", produces = { MediaType.APPLICATION_JSON_VALUE, })
	public ResponseEntity<List<Student>> getStudentsid(@RequestParam(name = "id", required = false) Integer id) {
		if (id != null) {
			List<Student> students = studentService.findStudentById(id);
			return ResponseEntity.status(200).body(students);
		} else {
			List<Student> students = studentService.getAllStudents();
			return ResponseEntity.status(200).body(students);

		}
	}

	@PutMapping("students/{id}")
	public ResponseEntity<Student> updateStudent(@PathVariable("id") Integer id, @RequestBody Student student) {
		studentService.updateStudent(id, student);
		return ResponseEntity.status(200).body(student);
	}

	@DeleteMapping("students/{id}")
	public void deleteStudent(@PathVariable("id") Integer id) {
		studentService.deleteStudent(id);
		// return ResponseEntity.status(201);

	}

	@PatchMapping("students/{id}")
	public ResponseEntity<Student> partialUpdateStudent(@PathVariable("id") Integer id, @RequestBody Student student) {
		Student updatedStudent = studentService.partialStudentUpdate(id, student);
		return ResponseEntity.status(200).body(updatedStudent);
	}

	@GetMapping(value = "/students2", produces = { MediaType.APPLICATION_JSON_VALUE, })
	public ResponseEntity<List<Student>> getStudentsidAndAge(@RequestParam(name = "id", required = false) Integer id,
			@RequestParam(name = "age", required = false) Integer age) {
		if (id != null && age != null) {
			List<Student> students = studentService.findStudentByIdAndAge(id, age);
			return ResponseEntity.status(200).body(students);
		} else {
			List<Student> students = studentService.getAllStudents();
			return ResponseEntity.status(200).body(students);

		}
	}

	@GetMapping(value = "/students3", produces = { MediaType.APPLICATION_JSON_VALUE, })
	public ResponseEntity<List<Student>> getStudentsidOrAge(@RequestParam(name = "id", required = false) Integer id,
			@RequestParam(name = "age", required = false) Integer age) {
		if (id != null || age != null) {
			List<Student> students = studentService.findStudentByIdOrAge(id, age);
			return ResponseEntity.status(200).body(students);
		} else {
			List<Student> students = studentService.getAllStudents();
			return ResponseEntity.status(200).body(students);

		}
	}

	@GetMapping(value = "/students4", produces = { MediaType.APPLICATION_JSON_VALUE, })
	public ResponseEntity<List<Student>> getStudentsByAgeLessThan(
			@RequestParam(name = "age", required = false) Integer age) {
		if (age != null) {
			List<Student> students = studentService.findStudentByAgeLessThan(age);
			return ResponseEntity.status(200).body(students);
		} else {
			List<Student> students = studentService.getAllStudents();
			return ResponseEntity.status(200).body(students);

		}
	}

	@GetMapping(value = "/students5", produces = { MediaType.APPLICATION_JSON_VALUE, })
	public ResponseEntity<List<Student>> getStudentsByAgeGreaterThanEqual(
			@RequestParam(name = "age", required = false) Integer age) {
		if (age != null) {
			List<Student> students = studentService.findStudentByAgeGreaterThanEqual(age);
			return ResponseEntity.status(200).body(students);
		} else {
			List<Student> students = studentService.getAllStudents();
			return ResponseEntity.status(200).body(students);

		}
	}

	@GetMapping(value = "/students6", produces = { MediaType.APPLICATION_JSON_VALUE, })
	public ResponseEntity<List<Student>> getStudentsByAgeLessThanEqual(
			@RequestParam(name = "age", required = false) Integer age) {
		if (age != null) {
			List<Student> students = studentService.findStudentByAgeLessThanEqual(age);
			return ResponseEntity.status(200).body(students);
		} else {
			List<Student> students = studentService.getAllStudents();
			return ResponseEntity.status(200).body(students);

		}
	}

	@GetMapping(value = "/students7", produces = { MediaType.APPLICATION_JSON_VALUE, })
	public ResponseEntity<List<Student>> getStudentsByAgeGreaterThan(
			@RequestParam(name = "age", required = false) Integer age) {
		if (age != null) {
			List<Student> students = studentService.findStudentByAgeGreaterThan(age);
			return ResponseEntity.status(200).body(students);
		} else {
			List<Student> students = studentService.getAllStudents();
			return ResponseEntity.status(200).body(students);

		}
	}

	@GetMapping(value = "/students8", produces = { MediaType.APPLICATION_JSON_VALUE, })
	public ResponseEntity<List<Student>> getStudentsByNameIsNull() {
		List<Student> students = studentService.findStudentByNameIsNull();
		return ResponseEntity.status(200).body(students);

	}

	@GetMapping(value = "/students9", produces = { MediaType.APPLICATION_JSON_VALUE, })
	public ResponseEntity<List<Student>> getStudentsByAgeBetweenl(
			@RequestParam(name = "fromAge", required = true) Integer fromAge,
			@RequestParam(name = "toAge", required = true) Integer toAge) {
		List<Student> students = studentService.findStudentByAgeBetween(fromAge, toAge);
		return ResponseEntity.status(200).body(students);
	}

	@GetMapping(value = "/students10", produces = { MediaType.APPLICATION_JSON_VALUE, })
	public ResponseEntity<List<Student>> getStudentsByNameIs(
			@RequestParam(name = "name", required = false) String name) {
		List<Student> students = studentService.findStudentByNameIs(name);
		return ResponseEntity.status(200).body(students);
	}

	@GetMapping(value = "/students11", produces = { MediaType.APPLICATION_JSON_VALUE, })
	public ResponseEntity<List<Student>> getStudentsByNameEquals(
			@RequestParam(name = "name", required = false) String name) {
		List<Student> students = studentService.findStudentByNameEquals(name);
		return ResponseEntity.status(200).body(students);
	}

	@GetMapping(value = "/students12", produces = { MediaType.APPLICATION_JSON_VALUE, })
	public ResponseEntity<List<Student>> getStudentsByNameIsNot(
			@RequestParam(name = "name", required = false) String name) {
		List<Student> students = studentService.findStudentByNameIsNot(name);
		return ResponseEntity.status(200).body(students);
	}

	@GetMapping(value = "/students13", produces = { MediaType.APPLICATION_JSON_VALUE, })
	public ResponseEntity<List<Student>> getStudentsByNameIsNotNull() {
		List<Student> students = studentService.findStudentByNameIsNotNull();
		return ResponseEntity.status(200).body(students);
	}

	@GetMapping(value = "/students14", produces = { MediaType.APPLICATION_JSON_VALUE, })
	public ResponseEntity<List<Student>> getStudentsByNameLike() {
		List<Student> students = studentService.findStudentByNameLike("b%");
		return ResponseEntity.status(200).body(students);
	}

	@GetMapping(value = "/students15", produces = { MediaType.APPLICATION_JSON_VALUE, })
	public ResponseEntity<List<Student>> getStudentsAllByOrderById() {
		List<Student> students = studentService.findStudentAllByOrderById();
		return ResponseEntity.status(200).body(students);
	}

}
