package com.example.swagger;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;


@Service


	@Repository
	public interface StudentRepository extends JpaRepository<Student, Integer> {
		public List<Student> findByAge(int age);
		public List<Student> findById(int id);
		public List<Student> findByIdAndAge(int id, int age);
		public List<Student> findByIdOrAge(int id, int age);
		public List<Student> findByAgeLessThan(int age);
		public List<Student> findByAgeLessThanEqual(int age);
		public List<Student> findByAgeGreaterThanEqual(int age);
		public List<Student> findByAgeGreaterThan(int age);
		public List<Student> findByAgeIsNull();
		public List<Student> findByNameIsNull();
		public List<Student> findByAgeBetween(int from, int to);
		public List<Student> findByNameIs(String name);
		public List<Student> findByNameEquals(String name);
		public List<Student> findByNameIsNot(String name);
		public List<Student> findByNameIsNotNull();
		public List<Student> findByNameLike(String likePattern);
		public List<Student> findAllByOrderById();
		public List<Student> findAllByOrderByIdAsc( );
		public List<Student> findAllByOrderByIdDesc();
	}

